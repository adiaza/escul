require 'smarter_csv'
filename = File.expand_path('../psu.csv', __FILE__)
f = File.open(filename, 'r:bom|utf-8')

options = {
  col_sep: ';',
  key_mapping:
  {
    n: :quantity,
    promedio: :average,
    maximo: :max,
    minimo: :min,
    categoria: :psu_test_id,
    rbd: :school_id,
    agno: :year
  }
}

n = SmarterCSV.process(f, options) do |array|
  # we're passing a block in, to process each resulting hash / =row
  # (the block takes array of hashes)
  # when chunking is not enabled, there is only one hash in each array
  Psu.create(array.first)
end
f.close