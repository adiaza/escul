require 'smarter_csv'
filename = File.expand_path('../colegios.csv', __FILE__)
f = File.open(filename, 'r:bom|utf-8')

options = {
  chunk_size: 2000,
  col_sep: ';',
  key_mapping:
  {
    agno: :year,
    nom_rbd: :name,
    rbd: :id,
    cod_com_rbd: :town_id,
    rural_rbd: :rural,
    fono_rbd: :phone,
    celular_rbd: :mobile,
    dir_rbd: :address,
    latitud: :latitude,
    longitud: :longitude,
    cod_depe: :dependency_id,
    convenio_pie: nil,
    ori_religiosa: :religion_id,
    ORI_OTRO_GLOSA: nil,
    pago_matricula: :tuition_fee_id,
    pago_mensual: :monthly_fee_id
  }
}

n = SmarterCSV.process(f, options) do |chunk|
  # we're passing a block in, to process each resulting hash / =row
  # (the block takes array of hashes)
  # when chunking is not enabled, there is only one hash in each array
  School.create(chunk)
end
f.close

# AGNO;RBD;NOM_RBD;COD_COM_RBD;COD_DEPE;RURAL_RBD;FONO_RBD;CELULAR_RBD;DIR_RBD;LATITUD;LONGITUD;CONVENIO_PIE;ORI_RELIGIOSA;ORI_OTRO_GLOSA;PAGO_MATRICULA;PAGO_MENSUAL
