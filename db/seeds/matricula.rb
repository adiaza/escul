require 'smarter_csv'
f = File.expand_path('../matricula.csv', __FILE__)

options = {
  chunk_size: 2000,
  col_sep: ';'
}

n = SmarterCSV.process(f, options) do |chunk|
  # we're passing a block in, to process each resulting hash / =row
  # (the block takes array of hashes)
  # when chunking is not enabled, there is only one hash in each array
  Enrollment.create(chunk)
end