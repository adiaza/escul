require 'smarter_csv'
filename = File.expand_path('../simce.csv', __FILE__)
f = File.open(filename, 'r:bom|utf-8')

options = {
  col_sep: ';',
  key_mapping:
  {
    rbd: :school_id,
    agno: :year,
    alum: :quantity,
    prom: :average,
    difgru: :dif_gse,
    siggru: :sig_gse,
    codigo_asignatura: :simce_test_id,
    grado: :grade_name_id,
    dif: :dif,
    sig: :sig,
    note: :note
  }
}

n = SmarterCSV.process(f, options) do |array|
  # we're passing a block in, to process each resulting hash / =row
  # (the block takes array of hashes)
  # when chunking is not enabled, there is only one hash in each array
  Simce.create(array.first)
end
f.close