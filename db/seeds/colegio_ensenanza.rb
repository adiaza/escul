require 'smarter_csv'
f = File.expand_path('../colegio_ensenanza.csv', __FILE__)

options = {
  chunk_size: 2000,
  col_sep: ';',
  key_mapping:
  {
    agno: :year,
    rbd: :school_id,
    ens: :level_id
  }
}

n = SmarterCSV.process(f, options) do |chunk|
  # we're passing a block in, to process each resulting hash / =row
  # (the block takes array of hashes)
  # when chunking is not enabled, there is only one hash in each array
  SchoolLevel.create(chunk)
end
