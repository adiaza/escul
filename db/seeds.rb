# This file should contain all the record creation needed to seed the database
# with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db
# with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
require File.expand_path('../seeds/geo', __FILE__)
require File.expand_path('../seeds/dependencias', __FILE__)
require File.expand_path('../seeds/rango_pagos', __FILE__)
require File.expand_path('../seeds/religion', __FILE__)
require File.expand_path('../seeds/gse', __FILE__)
require File.expand_path('../seeds/ensenanzas', __FILE__)
require File.expand_path('../seeds/schools', __FILE__)
require File.expand_path('../seeds/colegio_ensenanza', __FILE__)
require File.expand_path('../seeds/categorias_psu', __FILE__)
require File.expand_path('../seeds/psu', __FILE__)
require File.expand_path('../seeds/categorias_simce', __FILE__)
require File.expand_path('../seeds/nombre_grados', __FILE__)
require File.expand_path('../seeds/simce', __FILE__)
require File.expand_path('../seeds/matricula', __FILE__)