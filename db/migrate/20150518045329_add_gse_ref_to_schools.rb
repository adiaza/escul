class AddGseRefToSchools < ActiveRecord::Migration
  def change
    add_reference :schools, :gse, index: true
    add_foreign_key :schools, :gses
  end
end
