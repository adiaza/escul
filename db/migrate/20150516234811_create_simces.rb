class CreateSimces < ActiveRecord::Migration
  def change
    create_table :simces do |t|
      t.references :school, index: true
      t.references :grade_name, index: true
      t.integer :quantity
      t.integer :year
      t.references :simce_test, index: true
      t.integer :average
      t.integer :dif
      t.integer :sig
      t.integer :dif_gse
      t.integer :sig_gse
      t.boolean :note

      t.timestamps null: false
    end
    add_foreign_key :simces, :schools
    add_foreign_key :simces, :grade_names
    add_foreign_key :simces, :simce_tests
  end
end
