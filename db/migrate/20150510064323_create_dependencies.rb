class CreateDependencies < ActiveRecord::Migration
  def change
    create_table :dependencies do |t|
      t.string :long_name
      t.string :name

      t.timestamps null: false
    end
  end
end
