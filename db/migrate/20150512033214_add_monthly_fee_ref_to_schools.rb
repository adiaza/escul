class AddMonthlyFeeRefToSchools < ActiveRecord::Migration
  def change
    add_reference :schools, :monthly_fee, index: true
    add_foreign_key :schools, :price_ranges, column: :monthly_fee_id
  end
end
