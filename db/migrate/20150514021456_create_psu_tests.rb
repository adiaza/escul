class CreatePsuTests < ActiveRecord::Migration
  def change
    create_table :psu_tests do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
