class CreatePriceRanges < ActiveRecord::Migration
  def change
    create_table :price_ranges do |t|
      t.integer :min
      t.integer :max
      t.string :name

      t.timestamps null: false
    end
  end
end
