class AddTuitionFeeRefToSchools < ActiveRecord::Migration
  def change
    add_reference :schools, :tuition_fee, index: true
    add_foreign_key :schools, :price_ranges, column: :tuition_fee_id
  end
end
