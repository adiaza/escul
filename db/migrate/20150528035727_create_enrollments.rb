class CreateEnrollments < ActiveRecord::Migration
  def change
    create_table :enrollments do |t|
      t.integer :boys_total
      t.integer :girls_total
      t.integer :total
      t.integer :simple_courses
      t.integer :mixed_courses
      t.integer :year
      t.references :school, index: true

      t.timestamps null: false
    end
    add_foreign_key :enrollments, :schools
  end
end
