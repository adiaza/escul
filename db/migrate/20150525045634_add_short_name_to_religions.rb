class AddShortNameToReligions < ActiveRecord::Migration
  def change
    add_column :religions, :short_name, :string
  end
end
