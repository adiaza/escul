class AddReligionRefToSchools < ActiveRecord::Migration
  def change
    add_reference :schools, :religion, index: true
    add_foreign_key :schools, :religions
  end
end
