class CreateGses < ActiveRecord::Migration
  def change
    create_table :gses do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
