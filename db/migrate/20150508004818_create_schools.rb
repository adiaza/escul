class CreateSchools < ActiveRecord::Migration
  def change
    create_table :schools do |t|
      t.integer :year
      t.integer :dgv_rbd
      t.string :name
      t.references :town, index: true
      t.boolean :rural
      t.integer :phone
      t.integer :mobile
      t.text :address
      t.string :latitude
      t.string :longitude
      t.boolean :convenio_pie
      t.string :ori_otro_glosa

      t.timestamps null: false
    end
    add_foreign_key :schools, :towns
  end
end
