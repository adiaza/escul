class CreatePsus < ActiveRecord::Migration
  def change
    create_table :psus do |t|
      t.references :school, index: true
      t.integer :year
      t.references :psu_test, index: true
      t.integer :quantity
      t.integer :max
      t.integer :min
      t.integer :average

      t.timestamps null: false
    end
    add_foreign_key :psus, :schools
    add_foreign_key :psus, :psu_tests
  end
end
