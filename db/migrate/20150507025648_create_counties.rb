class CreateCounties < ActiveRecord::Migration
  def change
    create_table :counties do |t|
      t.string :name
      t.references :state, index: true

      t.timestamps null: false
    end
    add_foreign_key :counties, :states
  end
end
