class AddDependencyRefToSchools < ActiveRecord::Migration
  def change
    add_reference :schools, :dependency, index: true
    add_foreign_key :schools, :dependencies
  end
end
