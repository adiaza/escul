class CreateLevels < ActiveRecord::Migration
  def change
    create_table :levels do |t|
      t.string :name
      t.string :long_name
      t.string :short_name

      t.timestamps null: false
    end
  end
end
