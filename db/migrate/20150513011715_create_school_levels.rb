class CreateSchoolLevels < ActiveRecord::Migration
  def change
    create_table :school_levels do |t|
      t.integer :level_id
      t.integer :school_id
      t.integer :year

      t.timestamps null: false
    end
  end
end
