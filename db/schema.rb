# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150528035727) do

  create_table "counties", force: :cascade do |t|
    t.string   "name"
    t.integer  "state_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "counties", ["state_id"], name: "index_counties_on_state_id"

  create_table "dependencies", force: :cascade do |t|
    t.string   "long_name"
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "enrollments", force: :cascade do |t|
    t.integer  "boys_total"
    t.integer  "girls_total"
    t.integer  "total"
    t.integer  "simple_courses"
    t.integer  "mixed_courses"
    t.integer  "year"
    t.integer  "school_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  add_index "enrollments", ["school_id"], name: "index_enrollments_on_school_id"

  create_table "grade_names", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "gses", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "levels", force: :cascade do |t|
    t.string   "name"
    t.string   "long_name"
    t.string   "short_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "price_ranges", force: :cascade do |t|
    t.integer  "min"
    t.integer  "max"
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "psu_tests", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "psus", force: :cascade do |t|
    t.integer  "school_id"
    t.integer  "year"
    t.integer  "psu_test_id"
    t.integer  "quantity"
    t.integer  "max"
    t.integer  "min"
    t.integer  "average"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "psus", ["psu_test_id"], name: "index_psus_on_psu_test_id"
  add_index "psus", ["school_id"], name: "index_psus_on_school_id"

  create_table "religions", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "short_name"
  end

  create_table "school_levels", force: :cascade do |t|
    t.integer  "level_id"
    t.integer  "school_id"
    t.integer  "year"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "schools", force: :cascade do |t|
    t.integer  "year"
    t.integer  "dgv_rbd"
    t.string   "name"
    t.integer  "town_id"
    t.boolean  "rural"
    t.integer  "phone"
    t.integer  "mobile"
    t.text     "address"
    t.string   "latitude"
    t.string   "longitude"
    t.boolean  "convenio_pie"
    t.string   "ori_otro_glosa"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.integer  "dependency_id"
    t.integer  "religion_id"
    t.integer  "monthly_fee_id"
    t.integer  "tuition_fee_id"
    t.integer  "gse_id"
  end

  add_index "schools", ["dependency_id"], name: "index_schools_on_dependency_id"
  add_index "schools", ["gse_id"], name: "index_schools_on_gse_id"
  add_index "schools", ["monthly_fee_id"], name: "index_schools_on_monthly_fee_id"
  add_index "schools", ["religion_id"], name: "index_schools_on_religion_id"
  add_index "schools", ["town_id"], name: "index_schools_on_town_id"
  add_index "schools", ["tuition_fee_id"], name: "index_schools_on_tuition_fee_id"

  create_table "simce_tests", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "simces", force: :cascade do |t|
    t.integer  "school_id"
    t.integer  "grade_name_id"
    t.integer  "quantity"
    t.integer  "year"
    t.integer  "simce_test_id"
    t.integer  "average"
    t.integer  "dif"
    t.integer  "sig"
    t.integer  "dif_gse"
    t.integer  "sig_gse"
    t.boolean  "note"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "simces", ["grade_name_id"], name: "index_simces_on_grade_name_id"
  add_index "simces", ["school_id"], name: "index_simces_on_school_id"
  add_index "simces", ["simce_test_id"], name: "index_simces_on_simce_test_id"

  create_table "states", force: :cascade do |t|
    t.string   "name"
    t.string   "roman_code"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "towns", force: :cascade do |t|
    t.string   "name"
    t.integer  "county_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "towns", ["county_id"], name: "index_towns_on_county_id"

end
