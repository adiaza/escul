class SchoolLevel < ActiveRecord::Base
  belongs_to :school
  belongs_to :level
end
