class Level < ActiveRecord::Base
  has_many :school_levels
  has_many :schools, through: :school_levels
end
