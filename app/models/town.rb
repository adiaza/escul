# Town = Comuna
class Town < ActiveRecord::Base
  belongs_to :county
  has_many :schools
end
