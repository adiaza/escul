# Colegio
class School < ActiveRecord::Base
  belongs_to :town
  belongs_to :dependency
  belongs_to :religion
  belongs_to :monthly_fee
  belongs_to :tuition_fee
  belongs_to :gse

  has_many :school_levels
  has_many :levels, through: :school_levels
  has_many :enrollments

  has_many :psus
  has_many :simces

  def self.search(search)
    # School.select(:id).where('name LIKE ?', "%#{search}%").first(20)
    School.where('name LIKE ?', "%#{search}%")
  end

  def students_per_course
    self.enrollments.first.total/self.enrollments.first.simple_courses
  end

  def genre
    enrollment = self.enrollments.first
    if enrollment.boys_total > 0 && enrollment.girls_total > 0
      { string: 'Mixto', icon: 'venus-mars' }
    elsif enrollment.boys_total > 0
      { string: 'Solo Hombres', icon: 'mars' }
    elsif enrollment.girls_total > 0
      { string: 'Solo Mujeres', icon: 'venus' }
    end
  end
end
