class Simce < ActiveRecord::Base
  belongs_to :school
  belongs_to :grade_name
  belongs_to :simce_test

  def sig?
    case self.sig
      when -1
        'Disminución significativa'
      when 0
        'Sin cambio significativo'
      when 1
        'Aumento significativo'
    end

  end

end
