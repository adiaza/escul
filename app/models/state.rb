# State = Region
class State < ActiveRecord::Base
  has_many :counties
end
