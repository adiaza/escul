class TuitionFee < ActiveRecord::Base
  self.table_name = 'price_ranges'
  has_many :schools
end
