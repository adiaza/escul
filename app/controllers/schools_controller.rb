class SchoolsController < ApplicationController
  def show
    @school = School.find(params[:id])
  end

  def index
    @schools = School.where('name LIKE ?', "%#{params[:q]}%")
  end

  def search
    @school = School.search params[:search]

    if @school.length > 1
      redirect_to schools_path(q: params[:search])
    elsif  @school.length == 1
      redirect_to @school
    else
      redirect_to root_url
    end

  end
end
